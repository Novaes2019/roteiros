O OBS é um software de streaming e gravação open source e gratuito. Com ele podemos gravar desde vídeos tutoriais, games e até fazer live streams.
Possui uma excelente performance, estabilidade e usabilidade. É utilizado pela maioria dos youtubers atualmente.

## Como instalar no windows
https://cdn-fastly.obsproject.com/downloads/OBS-Studio-25.0.4-Full-Installer-x64.exe

Baixar e instalar

## Ubuntu
sudo apt install ffmpeg
sudo apt install obs-studio

## Configurando
Saída - Gravação
Formato de saída: MP4
2 faixas de áudio
Taxa de bits: 2500 Kbps
Áudio - Taxa de amostragem: 48 kHz
Configurações avançadas = Escolher as faixas onde cada áudio irá ficar

### Melhorando o áudio
#### Filtro de rúido
* Limite de fecho = -45
* Limite de abertura = -35
#### Redução de ruídos
* Nível de redução = -30
#### Compressor
* Padrão

## Link útil
https://obsproject.com/