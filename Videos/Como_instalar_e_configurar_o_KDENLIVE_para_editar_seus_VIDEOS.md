O kdenlive é um software livre para edição de vídeos. Possui uma interface simples para trabalhar e uma grande possibilidade de mudanças. Nele você pode utilizar qualquer formato de áudio ou vídeo.
Você também pode utilizar efeitos, transições, backup automático e instalar add-ons, aumentando assim a gama de possibilidades.

# Como instalar no windows
Baixar e instalar

# Como instalar no ubuntu
sudo apt install kdenlive

# Dicas básicas
- Modificar em threads para o número de núcleos que seu processador possui, agilizando o tempo de renderização
- Aprender as teclas de atalho
- Mudar a velocidade do codificador para acelerar o processo
