# Introdução
+ https://joplinapp.org/  

Joplin é um software open source e gratuito para guardar notas e fazer lista de tarefas, similar ao Evernote, porém mantendo sua privacidade e funcionalidades.
Permite a sincronização em nuvem, para assim poder ver as notas de qualquer lugar, criptografia, ou seja, você só poderá ler as notas com a senha que foi definida e o melhor de tudo, tem suporte a edição em markdown.

# Links
Deixar roteiro no gitlab
Link sobre markdown

# Instalação
 + Windows: Baixar e usar
 + Linux: wget -O - https://raw.githubusercontent.com/laurent22/joplin/master/Joplin_install_and_update.sh | bash
 
# Pós-instalação
 + Explicar sobre criptografia
 + Mostrar sobre personalização
 + Mostrar sobre sincronização

# Fix bugs
 + unknown table
	+ Apagar pasta ~/.config/joplin-desktop
	+ Apagar pasta ~/.config/Joplin