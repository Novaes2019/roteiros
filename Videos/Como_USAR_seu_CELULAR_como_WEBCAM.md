Hoje iremos aprender a como utilizar o celular como webcam. O aplicativo que iremos utilizar é o DroidCam, precisaremos instala-lo no nosso celular e no nosso computador. Podemos utiliza-lo de várias maneiras, a que veremos hoje é com o auxílio do ADB.
# ADB no Linux
Para instalar o ADB no Ubuntu, utilizaremos o seguinte comando: 
```bash
sudo apt install android-tools-adb android-tools-fastboot
```
```bash
adb version
```
# Como usar o ADB no Windows
Para instalar o ADB no windows você precisa pesquisar por: ADB Drivers for (nome do celular). Encontrar o driver, baixar e instalar.

# Como instalar no linux
https://www.dev47apps.com/droidcam/linuxx/
Entre na pasta tmp
```bash
cd /tmp/
```
Baixe o zip contendo o Droidcam
```bash
wget https://files.dev47apps.net/linux/droidcam_latest.zip
```
Confira se o hash é o mesmo
```bash
echo "957e5ff9e1762d0e912ee43a5f9083bb droidcam_latest.zip" | md5sum -c --
``` 
(o retorno precisa ser SUCESSO ou OK)
Descompacte o arquivo baixado
```bash
unzip droidcam_latest.zip -d droidcam && cd droidcam
``` 
E instale
```bash
sudo ./install
``` 
Após isso, para abrir, basta digitar no seu terminal 
```bash
droidcam
``` 
# Como instalar no Windows
Baixe o executável e siga as instruções

# Como conectar
Para funcionar, você precisará, no seu celular, ativar a opção de USB Debugging ou Depuração USB. Com essa opção ativa, tente conectar via usb com o seu celular. Caso contrário, você precisará digitar em seu terminal o comando
```bash
adb kill-server
``` 
Seguido por
```bash
adb start-server
``` 
E ai aceitar o pedido de permissão no seu celular, após feito isso, basta conectar no Droidcam, lembrando de manter o aplicativo sempre aberto no seu celular!
Caso tiver dificuldades, acesse o site: https://www.dev47apps.com/droidcam/connect/